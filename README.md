# PVE Spice Connect

A `bash` script to easily connect to `Proxmox PVE` virtual machines' and containers' graphical interface using `virt-viewer` client and `SPICE` protocol.

**Note**: This script is targeted at Linux-based clients and as such is not suitable for Windows clients.

## Context

If you have installed Proxmox PVE, and created a virtual machine or a container with a `Spice` display and wanted to connect to it graphically, you probably [found out](https://pve.proxmox.com/wiki/SPICE) that if you want to use the UI of the virtual machine or container, you need to first connect to the Proxmox VE Management console, select the VM or container, then click on `Console`. 

Unfortunately, not all users have or are allowed to have access to the PVE Management console, and those having these rights, are then forced to use the VM's UI from within the PVE interface, in a browsers' tab.

Further, if you tried to connect to the PVE spice server using `virt-viewer`, you probably found out that an access token is required to authenticate on the spice server prior to connecting to a VM, and that then a spice connection file is required to have a successfull connection to the VM's display. 

As you may imagine, this process is not user-friendly at all.

## Project's objectives

In this context the purpose of this project are twofold:

1. to provide the guidelines to create PVE API Tokens and assign them to virtual machines in order to allow selected users to connect to these virtual machines remotely using `virt-viewer`.
2. to provide a script which will hide the complexity of the whole connection process to allow users to connect as seamlessly as possible to a remote VM from the command line interfafce (CLI) without having to use the Proxmox hypervisor UI anymore.

## Script's Features

- configuration through external configuration file
- automated search of a QEMU VM or LXC container in existing PVE nodes based on its `VMID`
- generation and retrieval of a spice configuration file to connect to the VM using `virt-viewer` 
- comprehensive error messages
- ability to configure Proxmox's spice server API host and port
- ability to start graphical console in fullscreen or not
- ability to enable USB redirection
- ability to enable USB autoshare
- ability to ignore certificates validation (if self signed CAs are not installed on the client device for instance)
- ability to change path where CA certificates are located
- ability to enable comprehensive debug logs

## Installation

### Required packages

The [`pvespiceconnect.sh`](pvespiceconnect.sh) script requires four dependencies to be installed in order to be executed.

- `git`, the client to clone the project and retrieve the code of the script and the example configuration file
- `bash`, the shell which will execute the script (usually already installed on any distribution)
- `curl`, a command-line tool used to transfer data in URLs, which will be used to communicate with PVE's spice server API
- `jq`, a lightweight JSON command line processor, which will be used to extract data from responses sent by the PVE's spice server API
- `virt-viewer`, a minimal tool for displaying the graphical console of a virtual machine, which will be used to communicate with the PVE's spice server through SPICE protocol.

You can install them on debian-based linux distributions using the `apt` command as follows:

```
sudo apt update
sudo apt upgrade
sudo apt install git bash curl jq virt-viewer
```

The next step will be to retrieve the [`pvespiceconnect.sh`](pvespiceconnect.sh) script and the [`config.env.example`](config.env.example) file.

### Retrieving the script

You can clone this project to retrieve locally its source code using the following command:

```
cd /path/to/the/directory/in/which/you/want/to/put/the/project
git clone https://gitlab.com/pawlakm/pve-spice-connect.git
```

## Configuration

### Proxmox VE configuration

Prior to configuring the script itself, we need to prepare Proxmox VE to allow remote users to connect to Proxmox VE's spice server.

Let's take the following use case: we have a user `spiceuser` and want it to be able to connect remotely to a PVE VM named `100` using the script provided by this project. 

In order to use the script you need to configure your PVE instance to:

- have a QEMU virtual machine or a LXC container with a virtual Display of type `SPICE` set in its `Hardware` section
- obtain a PVE API token
- obtain a PVE secret
- assign the token to a user and the VM

The first of these three elements is out of the scope of this documentation. In the following sections explain how to obtain an API token, a secret and how to configure the VM to allow `spiceuser` to connect remotely. Execute them in sequence.

**Note**: we consider here that you start with a blank PVE permissions configuration and that .

#### Connect to the PVE management console

1. Connect to your PVE management console as `root`

#### Create a permissions group for remote connection to the spice server

1. In the dropdown menu on the left, select `Server View`
2. Click on `Datacenter`, in the right pane a sub-menu should appear.
3. In the sub-menu, expand the `Permissions` item, multiple entries should appear.
4. Click on the `Groups` entry, in the right pane a table with three buttons above it should appear
5. Click on the `Create` button, a popup window should appear
6. In the `Name` field, enter `Spice`
7. Click on `Create`

#### Create a permissions role for remote connection to the spice server

1. In the dropdown menu on the left, select `Server View`
2. Click on `Datacenter`, in the right pane a sub-menu should appear.
3. In the sub-menu, expand the `Permissions` item, multiple entries should appear.
4. Click on the `Roles` entry, in the right pane a table with three buttons above it should appear
5. Click on the `Create` button, a popup window should appear
6. In the `Name` field, enter `Spice`
7. In the `Privileges` dropdown list, select `VM.Audit` and `VM.Console` (scroll down if needed, it is sufficient to click once on each entry, no need to press any key to enable multi-selection)
8. Click on `Create`

#### Create a `pve` permissions realm

1. In the dropdown menu on the left, select `Server View`
2. Click on `Datacenter`, in the right pane a sub-menu should appear.
3. In the sub-menu, expand the `Permissions` item, multiple entries should appear.
4. Click on the `Realms` entry, in the right pane a table with four buttons above it should appear
5. Click on the `Add` button, a dropdown list should appear
6. Click on `PVE`, a popup window should appear.
7. In the `Comment` field, enter `Proxmox VE authentication server`
8. Click on `Create`

#### Create a user which will be able to connect remotely to the spice server

1. In the dropdown menu on the left, select `Server View`
2. Click on `Datacenter`, in the right pane a sub-menu should appear.
3. In the sub-menu, expand the `Permissions` item, multiple entries should appear.
4. Click on the `Users` entry, in the right pane a table with five buttons above it should appear
5. Click on the `Add` button, a popup window should appear.
6. In the `Username` field, enter the username you want, for instance `spiceuser`
7. In the `Realm` field, select `Proxmox VE authentication server`
8. In the `Group` field, enter `Spice`
9. Click on `Add`

#### Create an API token for the user you created

1. In the dropdown menu on the left, select `Server View`
2. Click on `Datacenter`, in the right pane a sub-menu should appear.
3. In the sub-menu, expand the `Permissions` item, multiple entries should appear.
4. Click on the `API Tokens` entry, in the right pane a table with four buttons above it should appear
5. Click on the `Add` button, a popup window should appear.
6. In the `Username` field, select the username you created in the previous section
7. In the `Token ID` field, insert a token name to easily identify this token (for instance, name it `spice`)
8. Click on `Add`, a popup window should appear.
9. Write down the content of the `Token ID` field. If you used the values specified in the previous steps, it should be: `spiceuser@pve!spice`. This will be the value of the `TOKEN` environment variable in the script's configuration file
10. click on `Copy Secret Value` to copy the content of the `Secret` field and paste it somewhere safe. It should be in the form: `xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx`. This will be the value of the `SECRET` environment variable in the script's configuration file
11. You can close the popup window by clicking the `X`in the top right corner.

**Important**: The secret will only be displayed at this moment, there is no way to retrieve it afterwards. If you lose it you will need to re-execute the steps of this section again.

#### Assign the API token to each VM / CT to which the user must be able to connect to using Spice

1. In the dropdown menu on the left, select `Folder View`
2. Click on `Datacenter` to expand the list below it,
3. Click on `Virtual Machine` (or `LXC Container` if  to expand the list below it,
4. Click on the Virtual Machine (or Container) you want the user you created above to be able to connect to using Spice (in our example it's VM `100`). In the right pane a sub-menu should appear.
5. In the sub-menu, click on the `Permissions` item, in the right pane a table with two buttons above it should appear
6. Click on the `Add` button, a dropdown list should appear
7. Select `API Token Permission`, a popup window should appear.
8. In the `API Token` field, select the Token ID you created in the previous section, for instance `spiceuser@pve!spice`
9. In the `Role` field, Select the role you created in the role creeation section above, for instance `Spice`)
10. Click on `Add`

#### Verify that the token has been correctly set

1. In the dropdown menu on the left, select `Server View`
2. Click on `Datacenter`, in the right pane a sub-menu should appear.
3. In the sub-menu, click on the `Permissions` item, in the right pane a table with two buttons above it should appear
4. In the table, if you used the values above, you should have a line whose `Path` is `/vms/100`, `User/Group/API Token` is `spiceuser@pve!spice` and `Role` is `Spice`.

Once these PVE configuration steps are done, we are ready to prepare the configuration file which will be used by [`pvespiceconnect.sh`](pvespiceconnect.sh) to connect to the VM.

### Spice connection script configuration 

The configuration of the [`pvespiceconnect.sh`](pvespiceconnect.sh) script is done through a configuration file. Usually, you need one configuration file per user per VM you want to connect to. You can have more than one file, if you want to have alternative connections. 

The name and extension of the configuration file are not important. Name your configuration files and place them where you want. Just make sure that their name is easily understandable. 

The content of the file is a list of values which are assigned to environment variables using the `=` character. One variable is set per line as follows: 

```
VARIABLE1=VALUE1
VARIABLE2=VALUE2
VARIABLE3=VALUE3
...
```

Both mandatory and optional environment variables and corresponding values are listed in the next sections. You can use the [`config.env.example`](config.env.example) file as a starting point for writing your own configuration file. Just copy [`config.env.example`](config.env.example) and modify it's values according to your needs. 

**Note**: there is no need to use quotes or to escape characters in the configuration file.


#### Mandatory Settings

Each configuration file **must** provide some mandatory setting. Failing to do so will result in an error. 

| Environment Variable | Default Value | Description                                                                                                                 | 
|----------------------|---------------|-----------------------------------------------------------------------------------------------------------------------------|
| VMID                 | none          | ID of the VM                                                                                                                |
| HOST                 | none          | the host providing the PVE's spice server api                                                                               |
| TOKEN                | none          | the token must use the format `admin@pve!vm_console`                                                                        |
| SECRET               | none          | the token secret that has been shown ONCE upon creating the Token, it is in the form `xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx` |


#### Optional Settings

Some settings are optional as they have a predefined default value. Defining these variables in a configuration file will override this default value. 

| Environment Variable   | Default Value | Description                                                                                                                            | 
|------------------------|---------------|----------------------------------------------------------------------------------------------------------------------------------------|
| PORT                   | 8006          | the port the PVE's spice server API listens to. By default it is 8006, unless you put a a reverse proxy in front of your PVE instance. |
| INSECURE               | 1             | set to 0 if the certificate is not self-signed or is installed in the certificates on the host on which the script is executed.        |
| CA_PATH                | none          | the path where CA certificates to be used are located (mandatory if INSECURE is set to 0)                                              |
| VERBOSE                | 0             | verbose flag, set to 1 if you want to print the connection configuration being used                                                    |
| FULLSCREEN             | 0             | Set to 1 to start in fullscreen mode and 0 otherwise                                                                                   |
| ENABLE_USB_REDIRECTION | 0             | Set to 1 to redirect host USB to the VM                                                                                                |
| ENABLE_USB_AUTOSHARE   | 0             | Set to 1 to share automatically USB with the VM when inserted locally                                                                  |


## Script Usage

Once you have your environment variable file name ready, you can launch the script as follows:

```
pvespiceconnect.sh <configuration.env>
```

For instance, if `vm100.env` is your configuration file, execute: 

```
pvespiceconnect.sh vm100.env
```

Or if it is not located in the same directory as the [`pvespiceconnect.sh`](pvespiceconnect.sh) script:

```
pvespiceconnect.sh /absolute/path/to/vm100.env
pvespiceconnect.sh ./relative/path/to/vm100.env
```

This will result in the script connecting to the VM using `remote-viewer` command from `virt-viewer` package using `vm100.env` as configuration inputs. 

Once launched, you should see an output similar to this one (with `VERBOSE` set to `0`):

```
[i] Starting PVE spice connection script...
[i] Loading environment variables from 'vm100.env' file
[i] Building PVE spice connection...
[i] Connecting remotely to VMID '100' on 'HOST:PORT'
[i] Remote viewer launched successfully.
```

## Debug Logging

By default, only minimal output is writtent to `std.out`. In case of error, explicit error messages are sent to `std.err` and are prepended with `[e]`. 

Should you need more information to correct an error, or if you need to follow the execution of the script, turn `VERBOSE` to `1` in the configuration file. In debug mode, all additional logs are prepended with `[d]`.

## Contributing

Open an issue and a merge request, feel free to contribute and to improve this work. Just make sure that if you add a feature, you also improve the documentation accordingly.

### Using logging helper functions

Few logging helper functions have been added to increase the readability of the code and readability of the logs. Should you improve the code, please use them whenever possible to keep the code clean and outputs understandable.

| Function   | Parameters                 | Description                                                      | Target stream |
|------------|----------------------------|------------------------------------------------------------------|---------------|
| log        | un-quoted text             | logs an INFO message                                             | std.out       |
| log_debug  | un-quoted text OR filename | logs a DEBUG mesage OR logs the content of a file at DEBUG level | std.out       |
| log_error  | un-quoted text             | logs an ERROR message                                            | std.err       |
| exit_error | none                       | logs an exit message as ERROR then exits with code 1             | std.err       |


## Credits

I am not the original creator of this script, but I improved it to better fit my needs. I added logging, inputs validation, error checking, increased configurability, toggle to bypass or not certificates validation, or change the location of CA files. I also wrote this documentation, a sample configuration file and created this gitlab repository with the hope to make the script easily retrievable, usable and improvable by others.

Therefor, other people deserve credits. This script is strongly inspired from the work and discussions of the Proxmox Forum community on the thread: "[Remote Spice access *without* using web manager](https://forum.proxmox.com/threads/remote-spice-access-without-using-web-manager.16561/)". The work of Rickard Osser <ricky@osser.se> and Jonas Stunkat <Proxmox.JS@it-quirks.de> which can be found [here](https://forum.proxmox.com/threads/remote-spice-access-without-using-web-manager.16561/page-6#post-373159) was used as a starting point. 

Environment variables loading command is taken from the [following answer on stackoverflow](https://stackoverflow.com/a/66118031/134904) by [Kolypto](https://stackoverflow.com/users/134904/kolypto).

I hope I forgot nobody, and that past contributors won't mind the decision of selecting the following license to protect this community work. If you think you also deserve to be listed here, just contact me.

## License

This project is licensed under the GNU General Public License Version 3 (See the [LICENSE](LICENSE) file).

